import React, { useState, useEffect } from 'react';
import { PageSections } from '../components/PageSections';
import { fetchContentSavings } from '../services/content';
import { flattenPageContent } from '../utils/content';

export const OnlineSavings = () => {
  const [content, setContent] = useState({})
  useEffect(() => {
    (async () => setContent(flattenPageContent(await fetchContentSavings())))()
  }, [setContent])
  return (
    <div>
      <PageSections content={content} />
    </div>
  )
}
