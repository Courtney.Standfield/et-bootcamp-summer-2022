import React, { useState, useEffect } from 'react';
import { PageSections } from '../components/PageSections';
import { fetchContentMoneyMarket } from '../services/content';
import { flattenPageContent } from '../utils/content';

export const MoneyMarket = () => {
  const [content, setContent] = useState({})
  useEffect(() => {
    (async () => setContent(flattenPageContent(await fetchContentMoneyMarket())))()
  }, [setContent])
  return (
    <div>
      <PageSections content={content} />
    </div>
  )
}
