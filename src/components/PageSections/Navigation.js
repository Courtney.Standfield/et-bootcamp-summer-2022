import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useCallback, useState } from 'react';

const navTextToSectionMap = {
	'pocFeaturesSection': 'Features',
	'pocBankBetter': 'Bank Better',
}
const UnstyledNavigation = ({ sections = [], className }) => {
	const [activeSection, setActiveSection] = useState(undefined, []);
	const handleClick = useCallback(({ target }) => {
		setActiveSection(target.name)

	}, [setActiveSection]);
  
	return (
		<ul className={className}>
			{sections.map(section => (
				<li key={`navigation-section-${section.id || section.contentType}`}><Link to={`#${section.contentType}`} name={section.contentType} onClick={handleClick} className={section.contentType === activeSection ? 'active' : ''}>{navTextToSectionMap[section.contentType]}</Link></li>
			))}
		</ul>
	);
}

export const Navigation = styled(UnstyledNavigation)`
  list-style-type: none;
  font-family: "Boing-Bold", "HelveticaNeue-Regular", "HelveticaNeue Regular", "Helvetica Neue", "Helvetica", Arial, "Lucida Grande", sans-serif;
  display: flex;
  align-items: center;
  a {
		color: #0071c4;
    text-decoration: none;
    &:visited {
      color: #0071c4;
    }
    &.active {
      border-bottom: solid 2px #006899;
    }
  }
  li {
    padding: 0 1rem;
  }
  color: #006899;
`