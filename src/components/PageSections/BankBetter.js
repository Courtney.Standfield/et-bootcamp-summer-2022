import React, { useMemo, useCallback } from 'react'
import styled from 'styled-components';

const UnstyledBankBetter = ({ content = {}, className }) => {
  const popAlert = useCallback((e) => {
    e.preventDefault();
    alert('clicked button');
  }, []);
  const { heading, buttonText, items, subheading } = content;
  const sortedItems = useMemo(() => items && items.sort((a, b) => a.order - b.order), [items]);
  return (
    <div className={className} id={content.contentType}>
      <h2>{heading}</h2>
      <h3>{subheading}</h3>
      <ul>
        {
          sortedItems && sortedItems.map((item) => (
            <li key={`${item.order}-${item.heading.substring(0, 10)}`}>
              <img src={item.headerImage.url} alt={item.headerImage.title} />
              <h4>{`${item.order}. ${item.heading}`}</h4>
              <p>{item.description}</p>
            </li>
          ))
        }
      </ul>
      <div>
        <a href="/" onClick={popAlert}>{buttonText}</a>
      </div>
    </div>
  );
}

export const BankBetter = styled(UnstyledBankBetter)`
  color: white;
  background: linear-gradient(#3798c7, #3B4FA8);
  font-family: "Lato", "HelveticaNeue-Regular", "HelveticaNeue Regular", "Helvetica Neue", "Helvetica", Arial, "Lucida Grande", sans-serif;
  text-align: center;
  ul {
    padding: 1rem;
    margin: 0;
    display: inline-block;
  }
  li {
    width: 33%;
    float: left;
    list-style-type: none;
    padding: 1rem 0 1rem 0;
  }
  h2 {
    font-size: 30px;
    padding: 0;
    margin: 0;
  }
  h3 {
    padding: 0;
    margin: .5rem;
    padding-bottom: .5rem;
  }
  h4 {
    font-size: 20px;
    font-weight: 400;
  }
  a {
    color: white;
    font-size: 1.1rem;
    border: 2px solid currentColor;
    line-height: 2rem;
    padding: .5rem 1rem .5rem 1rem;
    border-radius: 4px;
  }
  a:hover {
    cursor: pointer;
  }
  padding-top: 3rem;
  padding-bottom: 3rem;
`;