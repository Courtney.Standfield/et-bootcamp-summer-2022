import React from 'react'
import styled from 'styled-components';

const UnstyledFeatures = ({ content = {}, className }) => {
  const { heading, items } = content;
  return (
    <div className={className} id={content.contentType}>
      <h2>{heading}</h2>
      <ul>
        {
          items && items.map((item) => (
            <li key={Math.random()}>
              <img src={item.pocFeatureItemImage.url} alt={item.pocFeatureItemImage.title} />
              <div>
                <h3>{item.pocFeatureItemHeading}</h3>
                <p>{item.pocFeatureItemSubheading}</p>
              </div>
            </li>
          ))
        }
      </ul>
    </div>
  );
};

export const Features = styled(UnstyledFeatures)`
  color: white;
  background-color: #650360;
  font-family: "Lato", "HelveticaNeue-Regular", "HelveticaNeue Regular", "Helvetica Neue", "Helvetica", Arial, "Lucida Grande", sans-serif;
  padding-top: 2rem;
  padding-bottom: 2rem;
  ul {
    display: inline-block;
    padding: 1rem;
    margin-bottom: 0;
  }
  li {
    width: 30%;
    padding: 1rem;
    margin: 0;
    list-style-type: none;
    float: left;
    div {
      float: right;
      width: 80%;
      margin: 8px;
    }
  }
  h2 {
    font-size: 30px;
    padding: 0;
    text-align:center;
  }
  h3 {
    padding-top: 0;
    margin: 0;
  }
  img {
    float: left;
    width: 15%;
  }
`;